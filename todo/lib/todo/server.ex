defmodule Todo.Server do

  use GenServer, restart: :temporary
  @expiry_idle_timeout :timer.seconds(3600)

  def start_link(todo_list_name) do
    IO.puts("Starting to-do server for #{todo_list_name}")
    GenServer.start_link(__MODULE__, todo_list_name, name: global_name(todo_list_name))
  end

  @impl GenServer
  def init(name) do
    {
      :ok,
      {name, Todo.Database.get(name) || Todo.List.new()},
      @expiry_idle_timeout
    }
  end

  def whereis(name) do
    case :global.whereis_name({__MODULE__, name}) do
      :undefined -> nil
      pid -> pid
    end
  end

  def add_entry(todo_server_id, new_entry) do
    GenServer.cast(todo_server_id, {:add_entry, new_entry})
  end

  def entries(todo_server_id, date) do
    GenServer.call(todo_server_id, {:entries, date})
  end

  def update_entry(todo_server_id, entry) do
    GenServer.cast(todo_server_id, {:update_entry, entry})
  end

  def delete_entry(todo_server_id, entry_id) do
    GenServer.cast(todo_server_id, {:delete_entry, entry_id})
  end

  @impl GenServer
  def handle_cast({:add_entry, new_entry}, {todo_list_name, todo_list}) do
    new_list = Todo.List.add_entry(todo_list, new_entry)
    Todo.Database.store(todo_list_name, new_list)
    {:noreply, {todo_list_name, new_list}, @expiry_idle_timeout}
  end

  @impl GenServer
  def handle_cast({:update_entry, entry}, {todo_list_name, todo_list}) do
    {
      :noreply,
      {todo_list_name, Todo.List.update_entry(todo_list, entry)},
      @expiry_idle_timeout
    }
  end

  @impl GenServer
  def handle_cast({:delete_entry, entry_id}, {todo_list_name, todo_list}) do
    {
      :noreply,
      {todo_list_name, Todo.List.delete_entry(todo_list, entry_id)},
      @expiry_idle_timeout
    }
  end

  @impl GenServer
  def handle_call({:entries, date}, _, {todo_list_name, todo_list}) do
    {
      :reply,
      Todo.List.entries(todo_list,date),
      {todo_list_name, todo_list},
      @expiry_idle_timeout
    }
  end 

  defp global_name(name) do
    {:global, {__MODULE__, name}}
  end

  @impl GenServer
  def handle_info(:timeout, {name, todo_list}) do
    IO.puts("Stopping to-do server for #{name}")
    {:stop, :normal, {name, todo_list}}
  end
end

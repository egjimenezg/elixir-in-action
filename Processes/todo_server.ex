defmodule TodoServer do
  def start do
    spawn(fn -> loop(TodoList.new) end)
    |> Process.register(:todo_server)
  end

  defp loop(todo_list) do
    new_todo_list = receive do
      message ->
        process_message(todo_list, message)
    end
    
    loop(new_todo_list)
  end  

  def add_entry( new_entry) do
    send(:todo_server, {:add_entry, new_entry})
  end

  def entries( date) do
    send(:todo_server, {:entries, self(), date})
  
    receive do
      {:todo_entries, entries} -> entries
    after 5000 ->
      {:error, :timeout}
    end
  end

  def update_entry(entry) do
    send(:todo_server, {:update_entry, entry})
  end

  def delete_entry(entry_id) do
    send(:todo_server, {:delete_entry, entry_id})
  end

  defp process_message(todo_list, {:add_entry, new_entry}) do
    TodoList.add_entry(todo_list, new_entry)
  end

  defp process_message(todo_list, {:entries, caller, date}) do
    send(caller,{:todo_entries, TodoList.entries(todo_list,date)})
    todo_list
  end

  defp process_message(todo_list, {:update_entry, entry}) do
    TodoList.update_entry(todo_list, entry)
  end

  defp process_message(todo_list, {:delete_entry, entry_id}) do
    TodoList.delete_entry(todo_list, entry_id)
  end

end

defmodule TodoList do
  defstruct auto_id: 1, entries: Map.new

  def new(entries \\[]) do
    Enum.reduce(
      entries,
      %TodoList{},
      &add_entry(&2, &1)
    )
  end

  def add_entry(%TodoList{entries: entries,
                          auto_id: auto_id} = todo_list, entry) do
    entry = Map.put(entry, :id, auto_id)
    new_entries = Map.put(entries,auto_id,entry)

    %TodoList{todo_list | entries: new_entries,
                          auto_id: auto_id + 1 }
  end

  def entries(%TodoList{entries: entries}, date) do
    entries
    |> Stream.filter(fn({_,entry}) ->
         entry.date == date
       end)
    |> Enum.map(fn({_,entry}) ->
         entry
       end)
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn(_) -> new_entry end)
  end

  def update_entry(
    %TodoList{entries: entries} = todo_list,
    entry_id,
    updater_fun
  ) do

    case entries[entry_id] do
      nil -> todo_list

      old_entry ->
        old_entry_id = old_entry.id
        new_entry = %{id: ^old_entry_id} = updater_fun.(old_entry)
        new_entries = Map.put(entries, new_entry.id, new_entry)
        %TodoList{todo_list | entries: new_entries}
    end

  end

  def delete_entry(%TodoList{} = todo_list, entry_id) do
    %TodoList{todo_list | entries: Map.delete(todo_list.entries, entry_id) }
  end

end

defmodule TodoList.CsvImporter do

  def import(path_to_file) do
    if File.exists?(path_to_file) do
      path_to_file
      |> read_lines
      |> create_entries
      |> TodoList.new
    end
  end

  defp read_lines(file_name) do
    file_name
    |> File.stream!()
    |> Stream.map(&(String.replace(&1, "\n", "")))
  end
  
  defp create_entries(lines) do
    lines
    |> Stream.map(&extract_fields/1)
    |> Stream.map(&create_entry/1)
  end

  defp extract_fields(line) do
    line
    |> String.split(",")
    |> convert_date
  end

  defp convert_date([date_string, title]) do
    {parse_date(date_string), title}
  end

  defp parse_date(date_string) do
    [year,month,day] = 
      date_string
      |> String.split("/")
      |> Enum.map(&String.to_integer/1)

    {:ok, date} = Date.new(year, month, day)
    date
  end

  defp create_entry({date, title}) do
    %{date: date, title: title}
  end
end
